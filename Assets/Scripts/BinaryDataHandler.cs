﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class BinaryDataHandler
{
    /*
     * This class create the object of score and number of game count
     */

    public int maxScore;
    public int gameCount;

    public BinaryDataHandler(ScoreAndCountManager data)
    {
        maxScore = data.maxScore;
        gameCount = data.GameCount;
    }
}
