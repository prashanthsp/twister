using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;
using UnityEngine;

public static class BinarySaver 
{
    /*
     * This is static class store important game information like score and
     * game count in a binary format on Application persistentDataPath
     */
    readonly static string fileName = Application.persistentDataPath + "GameData.binary";

    public static BinaryDataHandler LoadGameData()
    {
        /*
         * This is static method call at the beginning of the game which read
         * binary data from file and Deserialize to BinaryDataHandler type
         */
        if (!File.Exists(fileName))
        {
            using(File.Create(fileName));
        }

        IFormatter formatter = new BinaryFormatter();
        FileStream file = File.Open(fileName, FileMode.Open);
        BinaryDataHandler gameData = (BinaryDataHandler)formatter.Deserialize(file);
        file.Close();
        return gameData;
    }

    public static void SaveGameData(BinaryDataHandler data)
    {
        /*
         * This is static method call at the end of the game when game over
         * store information such as score and game count to file in binary format
         */
        try
        {
            IFormatter formatter = new BinaryFormatter();
            FileStream saveFile = new FileStream(fileName, FileMode.Open);
            //FileStream saveFile = File.Create(fileName);
            formatter.Serialize(saveFile, data);
            saveFile.Close();
        }
        catch (System.Exception exception)
        {
            Debug.Log(exception);
        }
    }
}
