﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;


public class ControlScene : MonoBehaviour
{
    /*
     * This class organize scene as per user inputs
     */
    private TextMeshProUGUI score;
    private TextMeshProUGUI count;
    [SerializeField] TextMeshProUGUI scoreObject;
    [SerializeField] TextMeshProUGUI countObject;
    //private ScoreAndCountManager playerMetaObject = new ScoreAndCountManager();
    ScoreAndCountManager playerMetaObject;

    void Start()
    {
        playerMetaObject = FindObjectOfType<ScoreAndCountManager>();
        DisplayScoreAndCount();
    }

    public void LoadSettingScene()
    {
        /*
         * This method load Settings Scene
         */
        SceneManager.LoadScene(2);
    }

    public void LoadPlayScene()
    {
        /*
         * This method load Game scene
         */
        SceneManager.LoadScene(1);
    }

    public void GotoStartScene()
    {
        /*
         * This method load the start scene after game over
         */
        SceneManager.LoadScene(0);
    }

    public void ExitApplication()
    {
        /*
         * This method quit application
         */
        Application.Quit();
    }

    private void DisplayScoreAndCount()
    {
        /*
         * This method display best score and total game count on the beginning
         * game scene (scene index=0)
         */
        BinaryDataHandler data = BinarySaver.LoadGameData();
        if (SceneManager.GetActiveScene().buildIndex == 0)
        {
            scoreObject.text = data.maxScore.ToString();
            countObject.text = data.gameCount.ToString();
        }
    }
}
