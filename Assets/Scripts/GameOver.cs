﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class GameOver : MonoBehaviour
{
    /*
     * This class take care of the post ui elements after gameover
     */
    [SerializeField] private TextMeshProUGUI scoreText;
    [SerializeField] private TextMeshProUGUI maxScoreText;
    [SerializeField] private GameObject gameOver;
    [SerializeField] private GameObject gameOverBody;
    [SerializeField] private GameObject platforms;
    private bool isColorChange;
    ScoreAndCountManager playerMetaObject;


    void Start()
    {
        gameOver.SetActive(false);
    }
    
    public void UpdateGameOverUi()
    {
        /*
         * This method called from Player.method to show the current score and
         * best score if the current score > best score then it background 
         * color changes to cyan
         */
        playerMetaObject = FindObjectOfType<ScoreAndCountManager>();
        platforms.SetActive(false);
        gameOver.SetActive(true);
        isColorChange = (playerMetaObject.Score > playerMetaObject.MaxScore);
        if (isColorChange)
        {
            gameOverBody.GetComponent<Image>().color = Color.cyan;
        }
        scoreText.text = playerMetaObject.Score.ToString();
        maxScoreText.text = playerMetaObject.MaxScore.ToString();
    }

    public void RetryButtonHandler()
    {
        /*
         * This method used by Button gameobject which send response on click
         * then scene redirect to game start scene
         */
        SceneManager.LoadScene(0); 
    }

    public void QuitApplication()
    {
        /*
         * This method just quit the application
         */
        Application.Quit();
    }
}
