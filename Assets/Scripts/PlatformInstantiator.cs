﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PlatformInstantiator : MonoBehaviour
{
    /*
     * This class instantiate platfrom at runtime and update speed of the
     * platfrom wrt score
     */
    float totalLength;
    float checkStep;
    bool placeLeft = true;
    Vector3 rotation;
    private int scoreIndex = 0;
    List<int> scoreThreshold = new List<int>();
    List<GameObject> platforms = new List<GameObject>();
    [SerializeField] GameObject prefab;
    [SerializeField] float speed = 3;
    [SerializeField] public int numberOfPlatform;
    [SerializeField] float platformLength = 3;
    [SerializeField] float platformSpace = 1;
    [SerializeField] Vector3 lastPosition;
    ScoreAndCountManager playerMetaObject;

    void Start()
    {
        // This method called first time
        for (int index = 3; index < 10; index++)
        {
            scoreThreshold.Add(index * 10);
        }
        playerMetaObject = FindObjectOfType<ScoreAndCountManager>();
        totalLength = platformSpace + platformLength;
        // checkStep = -totalLength + (platformSpace / totalLength);
        checkStep = -1.4f;
        InitializePlaform();
    }

    void Update()
    {
        // This called for every frame
        UpdatePlatformSpeed();
    }

    private void InitializePlaform()
    {
        /*
         * This method Instantiate n-number of platform at the beginning at
         * x, y, z = (0, 0, 0) and place even number of platform normal
         * and odd numer of platform at + or - 45 degree alternatively
         */

        Vector3 position = new Vector3(0f, 0f, 0f);
        GameObject obj = Instantiate(prefab, transform);
        obj.transform.position = position;
        obj.name = "0";
        platforms.Add(obj);
        for (int index = 1; index < numberOfPlatform; index++)
        {
            GameObject platform;
            position.z += 1.2f;
            if (index % 2 == 0)
            {
                platform = Instantiate(prefab, transform);
                position.x = prefab.transform.position.x;
                // position.y = prefab.transform.position.y;
                platform.transform.position = position;
                platform.name = index.ToString();
                platforms.Add(platform);
            }
            else
            {

                platform = Instantiate(prefab, transform);

                if (placeLeft)
                {
                    // place platform at -.45 with respect to x-axis;
                    // Rotate platform to -45 degree 
                    position.x = prefab.transform.position.x + (-0.45f);
                    platform.transform.rotation = Quaternion.Euler(0f, 0f, -40f);
                    position.y = 0.2f;
                    placeLeft = false;
                }
                else
                {
                    // place platform at .45 with respect to x-axis;
                    // Rotate platfrom to 45 degree
                    position.x = prefab.transform.position.x + (0.45f);
                    platform.transform.rotation = Quaternion.Euler(0f, 0f, 40f);
                    position.y = 0.2f;
                    placeLeft = true;
                }

                platform.transform.position = position;
                position.y = 0f;
                platform.name = index.ToString();
                platforms.Add(platform);
            }
        }
        lastPosition = platforms[platforms.Count - 1].transform.position;
    }

    void FixedUpdate()
    {
        TranformPlatform();
    }

    private void TranformPlatform()
    {
        /* 
         * This method reposition every platform to last position after reaching
         * some threshold position
         */

        Color platformBgColor = Color.blue;
        var moveDelta = Vector3.back * speed * Time.fixedDeltaTime;
        for (int index = 0; index < platforms.Count; index++)
        {
            if (platforms[index].transform.position.z < checkStep)
            {
                lastPosition.x = platforms[index].transform.position.x;
                lastPosition.y = platforms[index].transform.position.y;
                platforms[index].transform.position = lastPosition;
            }
            else
            {
                platforms[index].transform.Translate(moveDelta);
            }
        }
    }

    private void UpdatePlatformSpeed()
    {
        /* 
         * This method increases platform movement speed once the score cross
         * some specific score threshold which is multiple of 10 from 3 to 9
         */

        if (playerMetaObject.Score > scoreThreshold[scoreIndex])
        {
            speed += 0.3f;
            if (scoreIndex < scoreThreshold.Count)
                scoreIndex++;
        }
    }
}
