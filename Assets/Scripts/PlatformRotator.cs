﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformRotator : MonoBehaviour
{
    /*
     * This class rotate and move platfrom to the specific position and angle
     * defined, and take user input 
     */
    private uint index = 1;
    private uint restrictRotation;
    private const uint totalRotation = 3;
    private int numberOfPlatform;
    Dictionary<uint, Quaternion> rotationAngles = new Dictionary<uint, Quaternion>();
    Dictionary<uint, Vector3> movePosition = new Dictionary<uint, Vector3>();
    PlatformInstantiator platformObject;
    [SerializeField] float rotationSpeed = 1f;

    void Start()
    {
        /*
         * movePosition stores position of the platfrom state when index 
         * increment or decrements 
         * rotationAngles store the angle of the platform state
         */
        platformObject = FindObjectOfType<PlatformInstantiator>();
        numberOfPlatform = platformObject.numberOfPlatform;
        rotationAngles.Add(0, Quaternion.Euler(0f, 0f, -45f));
        rotationAngles.Add(1, Quaternion.Euler(0f, 0f, 0f));
        rotationAngles.Add(2, Quaternion.Euler(0f, 0f, 45f));
        movePosition.Add(0, new Vector3(-0.48f, 0f, 0f));
        movePosition.Add(1, new Vector3(0f, 0f, 0f));
        movePosition.Add(2, new Vector3(0.48f, 0f, 0f));
    }

    void LateUpdate()
    {
        /*
         * This method calculate the restrictRotation to retrieve the
         * position and angle of the platfrom from Dictionary and call
         * RotateWrtIndex method 
         */
        restrictRotation = index % totalRotation;
        RotateWrtIndex();
    }

    private void RotateWrtIndex()
    {
        /*
         * This method rotate the platform root gameobject wrt to
         * restrictRotation stored in rotationAngles and movePosition Dict
         */
        transform.rotation = Quaternion.Lerp(
                                transform.rotation,
                                rotationAngles[restrictRotation],
                                rotationSpeed * Time.deltaTime
                            );

        transform.position = Vector3.Lerp(
                                transform.position,
                                movePosition[restrictRotation],
                                (rotationSpeed + 1) * Time.deltaTime
                            );

    }

    public void LeftButtonHandler()
    {
        /*
         * This button called when left side of the screen pressed on the
         * screen and index value update only when restrictRotation is >= 0
         * other wise index will not update just to restrict the rotation of
         * the platfrom
         */
        if (restrictRotation >= 0)
        {
            index++;
        }
    }

    public void RightButtonHandler()
    {
        /*
         * This button called when right side of the screen pressed on the
         * screen
         */
        if (restrictRotation <= 2)
        {
            index--;
        }
    }
}
