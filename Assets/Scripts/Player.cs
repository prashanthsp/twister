﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour
{
    /*
     * This class handle all functionality of ball
     */
    Rigidbody rigidBody;
    private Vector3 ballThrust; 
    private Vector3 offset;
    private Vector3 cameraCurrentPos;
    private ScoreAndCountManager playerMetaObject;
    private AudioSource sound;
    private bool soundEnable;
    private bool gameOverColor;
    private GameOver gameOverMetaObject;
    [SerializeField] GameObject cameraComponent;
    [SerializeField] float bounceForce = 1f;
    [SerializeField] TextMeshProUGUI scoreText;
    [SerializeField] Camera cameraColor;


    void Start()
    {
        gameOverMetaObject = FindObjectOfType<GameOver>();
        soundEnable = (PlayerPrefs.GetInt("Sound") == 1) ? true : false;
        sound = GetComponent<AudioSource>();
        playerMetaObject = FindObjectOfType<ScoreAndCountManager>();
        rigidBody = GetComponent<Rigidbody>();
    }

    void Update()
    {
        CheckForGameOver();
    }

    void LateUpdate()
    {
        /*
         * This method set ballThrust on every frame and calculate camera
         * offset, call UpdateCameraPosition() so camera follow ball on jump
         */
        ballThrust = Vector3.up * bounceForce * Time.deltaTime;
        offset = transform.position - cameraComponent.transform.position;
        UpdateCameraPosition();
    }

    private void UpdateCameraPosition()
    {
        //This method update camera position wrt ball position;
        cameraCurrentPos = cameraComponent.transform.position;
        cameraComponent.transform.position = new Vector3(
                                                cameraCurrentPos.x + offset.x,
                                                cameraCurrentPos.y,
                                                cameraCurrentPos.z
                                            );
    }

    private void CheckForGameOver()
    {
        /*
         * This method call every frame and check wheather ball reaches below
         * -1 wrt y-axis if it is then it stop game and call 
         *  GameOver.UpdateGameOverUi()
         */
        if (transform.position.y <= -1)
        {
            scoreText.text = "";
            playerMetaObject.GameCount += 1;
            playerMetaObject.CheckMaxScore();
            gameOverMetaObject.UpdateGameOverUi();
        }
    }

    void OnCollisionEnter(Collision other)
    {
        /*
         * This method is inherited from MonoBehaviour and checks the collision
         * if it occur then increment the score and update the score ui on the
         * screen
         */
        playerMetaObject.Score = playerMetaObject.Score + 1;
        scoreText.text = playerMetaObject.Score.ToString();
    }

    public void BallInputHandler()
    {
        /*
         * This method is called by player input game object on button press
         * and add force to the ball and also play the sound if user enabled
         * it on sound settings
         */
        if (soundEnable)
            sound.Play();

        rigidBody.AddForce(ballThrust, ForceMode.Impulse);
    }
}
