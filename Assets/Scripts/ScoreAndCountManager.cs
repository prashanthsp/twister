﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ScoreAndCountManager : MonoBehaviour
{
    /*
     * This class handle Score and Game count behaviour
     */
    private int score;
    public int maxScore;
    private int gameCount;

    void Start()
    {
        score = 0;
        LoadOnGameStart();
    }

    public int MaxScore 
    {
        get { return maxScore; }
    }

    public int Score
    {
        get { return score; }
        set { score = value; }
    }

    public int GameCount
    {
        get { return gameCount; }
        set { gameCount = value; }
    }

    public void CheckMaxScore()
    {
        /*
         * This method check wheather current score is greater than maxScore
         * if it is then assign score to maxScore
         */
        if (Score > maxScore)
        {
            maxScore = Score;
        }
        SaveOnGameEnd();
    }

    public void LoadOnGameStart()
    {
        /*
         * This method retrieve object from file store on disk and assign the 
         * maxscore and game count to maxScore and GameCount
         */
        BinaryDataHandler data = BinarySaver.LoadGameData();
        maxScore = data.maxScore;
        GameCount = data.gameCount;
    }

    public void SaveOnGameEnd()
    {
        /*
         * This method create BinaryDataHandler oject of current score and game
         * count ana save it by calling SaveGameData static method from static
         * BinarySaver class
         */
        BinaryDataHandler data = new BinaryDataHandler(this);
        BinarySaver.SaveGameData(data);
    }
}

