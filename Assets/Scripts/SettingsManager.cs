﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SettingsManager : MonoBehaviour
{
    /*
     * This class handle game settings
     */
    [SerializeField] private Toggle sound;
    private bool enable;

    void Start()
    {
        /*
         * This method retrieve previous sound setting given by user and assign
         * to current sound toggle component and call SoundSetting()
         */
        enable = System.Convert.ToBoolean(PlayerPrefs.GetInt("Sound"));
        sound.isOn = enable;
        SoundSetting();
    }

    public void SoundSetting()
    {
        /*
         * This method take user toggle input which is in bool and map to
         * equivalent 0 or 1 and store it in PlayerPrefs
         */
        PlayerPrefs.SetInt("Sound", (sound.isOn) ? 1 : 0);
        PlayerPrefs.Save();
    }
}
