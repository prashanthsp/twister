[Do]

    * Fix platform movement glitch [Added: 16/03/20]


[Done]

    * Score and Game count is now store in binary format on device [Completed: Before 16/03/20]
    * Start Scene completed [Completed: Before 16/03/20]
    * Add sound settings [Added: 16/03/20 Completed: 16/03/20]
    * GameOver UI [Added: 16/03/20 Completed: 17/03/20]
